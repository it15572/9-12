#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 11:09:12 2018

@author: imogentaylor
"""

# finite difference solver (Jacobi iteration) for the 2D Laplace equation
#   u_xx + u_yy = 0  0<x<Lx, 0<y<Ly
# with Dirichlet boundary conditions
#   u = fB(x) 0<=x<=Lx, y=0
#   u = fT(x) 0<=x<=Lx, y=Ly
#   u = fL(y) x=0, 0<=y<=Ly
#   u = fR(y) x=Lx, 0<=y<=Ly


import numpy as np
#import pylab as pl
from math import pi
import matplotlib.pyplot as plt



"""Boundary Conditions"""
def fB(x):
    # y=0 boundary condition
    u = np.zeros(x.size)
    return u

def fT(x):
    # y=Ly boundary condition
    u = np.sin(pi*x/Lx)
    return u

def fL(y):
    # x=0 boundary condition
    u = np.zeros(y.size)
    return u

def fR(y):
    # x=Lx boundary condition
    u = np.zeros(y.size)
    return u



"""Exact solution """
def u_exact(x,y):
    # the exact solution
    y = np.sin(pi*x/Lx)*np.sinh(pi*y/Lx)/np.sinh(pi*Ly/Lx)
    return y



""" Main function"""
def func_error(omega,maxerr,maxcount,Lx,Ly,mx,my):
    
    # set up the numerical environment variables
    x = np.linspace(0, Lx, mx+1)     # mesh points in x
    y = np.linspace(0, Ly, my+1)     # mesh points in y
    deltax = x[1] - x[0]             # gridspacing in x
    deltay = y[1] - y[0]             # gridspacing in y
    lambdasqr = (deltax/deltay)**2       # mesh number
    
    xx = np.append(x,x[-1]+deltax)-0.5*deltax  # cell corners needed for pcolormesh
    yy = np.append(y,y[-1]+deltay)-0.5*deltay
    
    # set up the solution variables
    u_old = np.zeros((x.size,y.size))   # u at current time step
    u_new = np.zeros((x.size,y.size))   # u at next time step
    u_true = np.zeros((x.size,y.size))  # exact solution
    R_old = np.zeros((x.size,y.size))
    
    # intialise the boundary conditions, for both timesteps
    u_old[1:-1,0] = fB(x[1:-1])
    u_old[1:-1,-1] = fT(x[1:-1])
    u_old[0,1:-1] = fL(y[1:-1])
    u_old[-1,1:-1] = fR(y[1:-1])
    u_new[:]=u_old[:]
    
    # true solution values on the grid 
    for i in range(0,mx+1):
        for j in range(0,my+1):
            u_true[i,j] = u_exact(x[i],y[j])
    
    # initialise the iteration
    err = maxerr+1
    count = 1
    # solve the PDE
    while err>maxerr and count<maxcount:
        for j in range(1,my):
            for i in range(1,mx):
                R_old[i,j] = (u_new[i-1,j]+u_old[i+1,j] - 2*(1+(lambdasqr))*u_old[i,j] + \
                     (lambdasqr)*u_new[i,j-1] + (lambdasqr)*u_old[i,j+1])/(2*(1+lambdasqr))
                u_new[i,j] = u_old[i,j] + omega*R_old[i,j]
    #            u_new[i,j] = ( u_new[i-1,j] + u_old[i+1,j] + \
    #                 lambdasqr*(u_new[i,j-1]+u_old[i,j+1]) )/(2*(1+lambdasqr))
                
        err = np.max(np.abs(u_new-u_old))
        u_old[:] = u_new[:]
        count=count+1
    
    # calculate the error, compared to the true solution    
    err_true = np.max(np.abs(u_new[1:-1,1:-1]-u_true[1:-1,1:-1]))

    return(err,count,err_true,u_new,xx,yy)
    
    
"""Prescribe parameter values and plot solution, outputting error and iterations """    
mx = 40 ; my=20 ; Lx=2.0 ; Ly=1.0 ; omega = 1.5; maxerr = 1e-4 ; maxcount = 1000
(err,count,err_true,u_new, xx, yy) = func_error(omega,maxerr,maxcount,Lx,Ly,mx,my)
# diagnostic output
print('Final iteration error =',err)
print('Iterations =',count)
print('Max diff from true solution =',err_true)

# and plot the resulting solution

plt.pcolormesh(xx,yy,u_new.T)
cb = plt.colorbar()
cb.set_label('u(x,y)')
plt.xlabel('x'); plt.ylabel('y')
plt.show()

""" Testing dependence of optimal omega on varying parameter values """
def optimal_dep(Lx,Ly,mx,my):
    omegas = np.linspace(1,2,11)
    iter_nums = np.zeros(len(omegas))
    for i in range(0,len(omegas)):
        iter_num = func_error(omegas[i],maxerr,maxcount,Lx,Ly,mx,my)[1]
        iter_nums[i] = iter_num
    pos = np.argmin(iter_nums)
    return (omegas[pos], min(iter_nums))

"""Plots showing dependence, separate for mx,my,Lx,Ly """
mxs = np.linspace(10,100,10)
mx_iters = np.zeros(len(mxs))
optimal_omegas_mx = np.zeros(len(mxs))
for i in range(0,len(mxs)):
    mx = int(mxs[i])
    optimal_omegas_mx[i], mx_iters[i] = optimal_dep(Lx,Ly,mx,my)

plt.plot(mxs,optimal_omegas_mx, 'bo')
plt.title('Optimal omega dependence on mx')
plt.xlabel('mx')
plt.ylabel('Optimal omega')
plt.savefig('mxdep.png')


mys = np.linspace(10,100,10)
my_iters = np.zeros(len(mys))
optimal_omegas_my = np.zeros(len(mys))
for i in range(0,len(mys)):
    my = int(mys[i])
    optimal_omegas_my[i], my_iters[i] = optimal_dep(Lx,Ly,mx,my)
    
plt.plot(mys,optimal_omegas_my,'ro')
plt.title('Optimal omega dependence on my')
plt.xlabel('my')
plt.ylabel('Optimal omega')
plt.savefig('mydep.png')

Lxs =  np.linspace(1,10,10)
Lx_iters = np.zeros(len(Lxs))
optimal_omegas_Lx = np.zeros(len(Lxs))
for i in range(0,len(Lxs)):
    Lx = int(Lxs[i])
    optimal_omegas_Lx[i],Lx_iters[i] = optimal_dep(Lx,Ly,mx,my)
    
plt.plot(Lxs,optimal_omegas_Lx,'go')
plt.title('Optimal omega dependence on Lx')
plt.xlabel('Lx')
plt.ylabel('Optimal omega')
plt.savefig('Lxdep.png')
    

Lys = np.linspace(1,10,10)
Ly_iters = np.zeros(len(Lys))
optimal_omegas_Ly = np.zeros(len(Lys))
for i in range(0,len(Lxs)):
    Ly = int(Lys[i])
    optimal_omegas_Ly[i], Ly_iters[i] = optimal_dep(Lx,Ly,mx,my)

plt.plot(Lys,optimal_omegas_Ly,'ko')
plt.title('Optimal omega dependence on Ly')
plt.xlabel('Ly')
plt.ylabel('Optimal omega')
plt.savefig('Lydep.png')