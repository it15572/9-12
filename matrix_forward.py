#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 17:25:40 2018

@author: imogentaylor
"""
import scipy as sp
import numpy as np


def matrix_forward_euler(lmbda,mx):
    return sp.sparse.diags([lmbda,(1-2*lmbda),lmbda],[-1,0,1],shape = (mx-1,mx-1))