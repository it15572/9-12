#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 17:32:05 2018

@author: imogentaylor
"""

import numpy as np
from scipy.sparse import diags
import math as mth
import pylab as pl
from matplotlib import pyplot as plt
from matplotlib import animation
#Design decision: keep h not as an input so that can define in a separate function
# h = 0 in open ocean, h = a_B*np.exp(-((x-x_B)**2)/(omega_B**2))
#                                   in the presence of an object.
#or keep h as an input to main function and write h1 and h2 and call either one depending on which bcs.

def h2(x,pars_B):
    return h_0

def h1(x,pars_B):
    return 10 - a_B*np.exp((-(x-x_B)**2)/(omega_B**2))

#Defining initial conditions:
def u(x,pars,pars_B):
    return h_0 + a_I*np.exp((-(x-x_I)**2)/(omega_I**2))

def u_t(x):
    return 0

def h3(x,pars_b):
    return h_0 - x

#define matrices
#define timestep function
#define main function 
#call it

#Design decision - want L to be large enough so that 
    #when using pde solver in presence of object, it can be far away enough from normal travelling wave.

#Design decision: writing loops to create vectors. Will then add these vectors to a sparse diagonal matrix a
def matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h):
    diag_0 = np.zeros(mx)
    diag_1 = np.zeros(mx+1)
    diag_2 = np.zeros(mx)
    for i in range(0,len(diag_0)):
        diag_0[i] = (Delta**2)*h(x[i] - deltax/2,pars_B)
        diag_2[i] = (Delta**2)*h(x[i] + deltax/2,pars_B)
    for i in range(0,len(diag_1)):
        diag_1[i] = 2 - (Delta**2)*(h(x[i] - deltax/2,pars_B) + h(x[i] + deltax/2,pars_B))
    diag_2[0] = (Delta**2)*(h(x[0] - deltax/2,pars_B) + h(x[0] + deltax/2,pars_B))
    diag_0[-1] = (Delta**2)*(h(x[-1] - deltax/2,pars_B) + h(x[-1] + deltax/2,pars_B))
    diagonals = [diag_0,diag_1,diag_2]
    return diags(diagonals,[-1,0,1],format = 'csc')



def cd_variable(u_j,u_jm1,Delta,x,deltax):
    u_jm1[0] = u_jm1[0]/(1 + 2*Delta*h(x[0] - deltax/2,pars_B)/((h_0)**0.5))
    u_jm1[-1] = u_jm1[-1]/(1 + 2*Delta*h(x[-1] + deltax/2,pars_B)/((h_0)**0.5))
    return (A_v_gen).dot(u_j) - u_jm1

def timestep_1(u_jm1):
    return 0.5*(A_v).dot(u_jm1)
    
def solve_pde_hyperbolic_varyspeed(L,T,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h):
    x = np.linspace(0,L, mx+1)     # gridpoints in space
    t = np.linspace(0,T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
#    lmbda = h(x,pars_B)*deltat/deltax         # squared courant number
    lmbda = (h_0**(1/2))*deltat/deltax
    Delta = (deltat/deltax)
    print("lambda=",lmbda)
    epsilon = 0.5

    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    
    
    global A_v, A_v_gen
    A_v = matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h)
    A_v_gen = matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h)
    e = 2*(h(x[0],pars_B))*Delta/((h_0)**(1/2))
    f = 2*(h(x[-1],pars_B))*Delta/((h_0)**(1/2))
    A_v_gen[0,0] = (2 - (Delta**2)*(h(x[0] - deltax/2,pars_B) + h(x[0] + deltax/2,pars_B))  + e)/(1+e)
    A_v_gen[0,1] = (Delta**2)*(h(x[0] - deltax/2,pars_B) + h(x[0] + deltax/2,pars_B))/(1+e)
    A_v_gen[-1,-1] = (2 - (Delta**2)*(h(x[-1] - deltax/2,pars_B) + h(x[-1] + deltax/2,pars_B)) + f)/(1+f)
    A_v_gen[-1,-2] = (Delta**2)*(h(x[-1] - deltax/2,pars_B) + h(x[-1] + deltax/2,pars_B))/(1+f)
    #Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = init_u(x[i],pars,pars_B)    
        
    #first time step:
    u_j = timestep_1(u_jm1)
#    u_j[0] = (u_jm1[0])*(1-lmbda) + lmbda*(u_jm1[1])
#    u_j[mx] = (u_jm1[mx])*(1-lmbda) + lmbda*(u_jm1[mx-1])
    
    
    for n in range(2,mt+1):
        #rewrite outermost elements of u_jm1 here so that this is used in general timestep

        u_jp1 = timestep_method(u_j,u_jm1,Delta,x,deltax)
        if u_jp1[-1] > h_0 + epsilon:
            u_jp1[0] = u_jp1[-1]
        else:
            u_jp1 = u_jp1
#        u_jp1[0] = (u_j[0])*(1-lmbda) + lmbda*(u_j[1])
#        u_jp1[mx] = (u_j[mx])*(1-lmbda) + lmbda*(u_j[mx-1])
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]  
        
        
        
        
    return (x,u_jp1,deltax)
#when calling: choose init_u = u, init_v = u_t

L = 20 ; T = 6.0 
mx = 100 ; mt = 3000 ; timestep_method = cd_variable 
init_u = u ; init_v = u_t
a_I= 5 ; x_I = 6 ; omega_I = 1
a_B = 0.5 ; x_B = 17 ; omega_B = 1
pars = (a_I,x_I,omega_I)
pars_B = (a_B,x_B,omega_B)
global h_0
h_0 = 5
h=h2
(x,u_jp1,deltax) = solve_pde_hyperbolic_varyspeed(L,T,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h)
pl.plot(x,u_jp1)
pl.axis([0, 20, 0, 12.5])
pl.xlabel('x')
pl.ylabel('u(x,T)')
pl.title('Wave motion at T=120 - open bc, constant h(x)')
#pl.savefig('open_constant_T120.png')
pl.show()