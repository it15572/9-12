#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 14:50:31 2018

@author: imogentaylor
"""

# simple forward Euler solver for the 1D heat equation
#   u_t = kappa u_xx  0<x<L, 0<t<T
# with zero-temperature boundary conditions
#   u=0 at x=0,L, t>0
# and prescribed initial temperature
#   u=u_I(x) 0<=x<=L,t=0

import numpy as np
import pylab as pl
from math import pi
from scipy import sparse
from scipy.sparse import linalg
from numpy import linalg as LA

"""Defining matrices used in computing unknown solutions for u_jp1, 
using different finite difference methods """

def matrix_forward_euler(lmbda,mx):
    h = sparse.diags([lmbda,(1-2*lmbda),lmbda],[-1,0,1],shape = (mx-1,mx-1), format = 'csc')
    return h

def matrix_backward_euler(lmbda,mx):
    h = sparse.diags([-lmbda,(1+2*lmbda),-lmbda],[-1,0,1],shape = (mx-1,mx-1), format ='csc')
    return h

def matrix_CN_1(lmbda,mx):
    h = sparse.diags([-lmbda/2,1+lmbda,-lmbda/2],[-1,0,1],shape=(mx-1,mx-1), format = 'csc')
    return h

def matrix_CN_2(lmbda,mx):
    h = sparse.diags([lmbda/2,1-lmbda,lmbda/2],[-1,0,1],shape=(mx-1,mx-1), format = 'csc')
    return h



"""Defining functions that are used to compute unknown solutions for u_jp1, 
using different finite difference methods and boundary conditions"""

#Different finite difference methods used for Dirichlet boundary conditions
def FE_nonhomdir(u_j,t,n,lmbda,deltax,deltat,b,p,q):            
    c= np.zeros(len(u_j[1:mx]))                         #defining new variable of size mx-1
    c[0] = p(t[n-1]) ; c[-1] = q(t[n-1])                #defining variables end points in terms of boundary conditions
    a = np.zeros(len(u_j))                              #defining new variable of size mx+1
    a[1:mx] = A1.dot(u_j[1:mx]) + lmbda*c + deltat*b    #assigning innermost points for u
    a[0] = p(t[n]) ; a[-1] = q(t[n])                    #assigning end poitns u_0 and u_m
    return a

def BE_nonhomdir(u_j,t,n,lmbda,deltax,deltat,b,p,q):
    c = np.zeros(len(u_j[1:mx]))
    c[0] = p(t[n]) ; c[-1] = q(t[n])
    a = np.zeros(len(u_j))
    a[1:mx] = linalg.spsolve(B1, u_j[1:mx] + lmbda*c + deltat*b)
    a[0] = p(t[n]) ; a[-1] = q(t[n])
    return a

def CN_nonhomdir(u_j,t,n,lmbda,deltax,deltat,b,p,q):
    c = np.zeros(len(u_j[1:mx]))
    c[0] = p(t[n]) + p(t[n-1]) ; c[-1] = q(t[n]) + q(t[n-1])
    a = np.zeros(len(u_j))
    a[1:mx] = linalg.spsolve(C1,D1.dot(u_j[1:mx]) + lmbda*0.5*c + deltat*b)
    a[0] = p(t[n]) ; a[-1] = q(t[n])
    return a


#Different finite difference methods used for Neumann boundary conditions
def FE_neum(u_j,t,n,lmbda,deltax,deltat,b,p,q):
    c= np.zeros(len(u_j))
    c[0] = -p(t[n-1]) ; c[-1] = q(t[n-1])
    return A2.dot(u_j) + 2*lmbda*deltax*c + deltat*b

def BE_neum(u_j,t,n,lmbda,deltax,deltat,b,p,q):
    c= np.zeros(len(u_j))
    c[0] = -p(t[n]) ; c[-1] = -q(t[n])
    return linalg.spsolve(B2, u_j + 2*lmbda*deltax*c)

def CN_neum(u_j,t,n,lmbda,deltax,deltat,b,p,q):
    c = np.zeros(len(u_j))
    c[0] = -(p(t[n]) + p(t[n-1])) ; c[-1] = q(t[n]) + q(t[n-1])
    return linalg.spsolve(C2,D2.dot(u_j) + lmbda*deltax*c + deltat*b)



"""Defining functions used to compute source term for both Dirichlet and Neumann Boundary conditions"""  

def matrix_RHS_neum(mx,s,x,func_RHS):
    b = np.zeros(mx+1)
    for i in range(0,mx+1):
        a = func_RHS(x[i],s)
        b[i] = a
    return b

def matrix_RHS_dir(mx,s,x,func_RHS):
    b = np.zeros(mx-1)
    for i in range(1,mx):
        a = func_RHS(x[i],s)
        b[i-1] = a
    return b




"""Defining main function to solve parabolic pdes"""

def solve_parabolic_pde_general(kappa,L,T,init_cond,mx,mt,timestep_method, matrix_RHS,p,q,func_RHS):
    #Defining discretised space
    x = np.linspace(0, L, mx+1)     # mesh points in space
    t = np.linspace(0, T, mt+1)     # mesh points in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = kappa*deltat/(deltax**2)   
    print("lambda=",lmbda)
    
    #Setting up dependent variables 
    u_j = np.zeros(len(x))        # u at current time step
    u_jp1 = np.zeros(len(x))      # u at next time step
    
    #Naming global matrices used in calculations    
    global A1,B1,C1,D1
    A1 = matrix_forward_euler(lmbda,mx)
    B1 = matrix_backward_euler(lmbda,mx)
    C1 = matrix_CN_1(lmbda,mx)
    D1 = matrix_CN_2(lmbda,mx)
    global A2,B2,C2,D2
    A2 = matrix_forward_euler(lmbda,mx+2)
    A2[0,1] = 2*lmbda ; A2[-1,-2] = 2*lmbda
    B2 = matrix_backward_euler(lmbda,mx+2)
    B2[0,1] = -2*lmbda ; B2[-1,-2] = -2*lmbda
    C2 = matrix_CN_1(lmbda,mx+2)
    C2[0,1] = -lmbda ; C2[-1,-2] = -lmbda
    D2 = matrix_CN_2(lmbda,mx+2)
    D2[0,1] = lmbda ; D2[-1,-2] = lmbda
    
    
    #Initial set of points u at time t=0
    for i in range(0, mx+1):
        u_j[i] = init_cond(x[i])

    
    #Finding set of points u at u = u(x,T)
    for n in range(1, mt+1):
    
        b = matrix_RHS(mx,n-1,x,func_RHS)                             #Source term at each time increment

        u_jp1 = timestep_method(u_j,t,n,lmbda,deltax,deltat,b,p,q)    #Compute u at next time increment
          
        u_j = u_jp1

    return (x,u_j)




"""Defining initial conditions and boundary conditions and source term"""

#exact solution
def u_exact(x,t):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

#Homogeneous Dirichlet bcs
def p1(t):  
    return 0
def q1(t):
    return 0    


#Nonhomogeneous Dirichlet bcs
def p2(t):
    return 10*t
def q2(t): 
    return -10*t


#Neumann bcs
def P1(t):
    return 0
def Q1(t):
    return 0



#Initial Conditions 
def u_I1(x):
    y = np.sin(pi*x/L)
    return y

def u_I2(x):
    y = (x-pi)**2
    return y 

def u_I3(x):
    y = (10*pi/L)*np.sin(pi*x/L)
    return y 



#Source term
def func_RHS_1(x,t):
    return 0

def func_RHS_2(x,t):
    return 10
 
    

"""Testing code"""

"""Homogeneous Dirichlet"""
#Defining parameter values 
kappa = 1.0 ; L = 2*pi ; T = 1.0 ; mx = 20 ; mt = 1000
#Call initial condition here
init_cond = u_I1
#Call boundary conditions here
p = p1 ; q = q1
#Call functions to use here
timestep_method = CN_nonhomdir
matrix_RHS = matrix_RHS_dir
func_RHS = func_RHS_1


#Calling main function
(x,u_j) = solve_parabolic_pde_general(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)

"""Nonhomogeneous Dirichlet"""

#Defining parameter values 
kappa = 1.0 ; L = 2*pi ; T = 1.0 ; mx = 20 ; mt = 1000
#Call initial condition here
init_cond = u_I1
#Call boundary conditions here
p = p2 ; q = q2
#Call functions to use here
timestep_method = CN_nonhomdir
matrix_RHS = matrix_RHS_dir
func_RHS = func_RHS_1


#Calling main function
(x2,u_j2) = solve_parabolic_pde_general(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)


""" Neumann """

#Defining parameter values 
kappa = 1.0 ; L = 2*pi ; T = 1.0 ; mx = 20 ; mt = 1000
#Call initial condition here
init_cond = u_I2
#Call boundary conditions here
p = P1 ; q = Q1
#Call functions to use here
timestep_method = CN_neum
matrix_RHS = matrix_RHS_neum
func_RHS = func_RHS_1


#Calling main function
(x3,u_j3) = solve_parabolic_pde_general(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)

""" Neumann plus source term """ 

#Defining parameter values 
kappa = 1.0 ; L = 2*pi ; T = 1.0 ; mx = 20 ; mt = 1000
#Call initial condition here
init_cond = u_I2
#Call boundary conditions here
p = P1 ; q = Q1
#Call functions to use here
timestep_method = CN_neum
matrix_RHS = matrix_RHS_neum
func_RHS = func_RHS_2


#Calling main function
(x4,u_j4) = solve_parabolic_pde_general(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)





"""Plotting functions"""
fig1,ax1 = pl.subplots()
ax1.plot(x,u_j,'ro', label = 'num')
pl.legend(loc='upper right')
ax1.set(xlabel='x',ylabel='u(x,T)', title = 'Homogeneous Dirichlet - Crank Nicholson')

fig2,ax2 = pl.subplots()
ax2.plot(x2,u_j2,'ro', label = 'num')
pl.legend(loc='upper right')
ax2.set(xlabel='x',ylabel='u(x,T)', title = 'Nonhomogeneous Dirichlet - Crank Nicholson')

fig3,ax3 = pl.subplots()
ax3.plot(x3,u_j3,'ro', label = 'num')
pl.legend(loc='upper right')
ax3.set(xlabel='x',ylabel='u(x,T)', title = 'Neumann - Crank Nicholson')

fig4,ax4 = pl.subplots()
ax4.plot(x4,u_j4,'ro', label = 'num')
pl.legend(loc='upper right')
ax4.set(xlabel='x',ylabel='u(x,T)', title = 'Neumann bcs with source term - Crank Nicholson')
pl.savefig('Neum_source_CN1.png')




""" Error analysis """ 
"""Equation used for error analysis """
def solve_parabolic_pde_general_error(kappa,L,T,init_cond,mx,mt,timestep_method, matrix_RHS,p,q,func_RHS):
    #Defining discretised space
    x = np.linspace(0, L, mx+1)     # mesh points in space
    t = np.linspace(0, T, mt+1)     # mesh points in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = kappa*deltat/(deltax**2)   
    print("lambda=",lmbda)
    
    #Setting up dependent variables 
    u_j = np.zeros(len(x))        # u at current time step
    u_jp1 = np.zeros(len(x))      # u at next time step
    
    #Naming global matrices used in calculations    
    global A1,B1,C1,D1
    A1 = matrix_forward_euler(lmbda,mx)
    B1 = matrix_backward_euler(lmbda,mx)
    C1 = matrix_CN_1(lmbda,mx)
    D1 = matrix_CN_2(lmbda,mx)
    global A2,B2,C2,D2
    A2 = matrix_forward_euler(lmbda,mx+2)
    A2[0,1] = 2*lmbda ; A2[-1,-2] = 2*lmbda
    B2 = matrix_backward_euler(lmbda,mx+2)
    B2[0,1] = -2*lmbda ; B2[-1,-2] = -2*lmbda
    C2 = matrix_CN_1(lmbda,mx+2)
    C2[0,1] = -lmbda ; C2[-1,-2] = -lmbda
    D2 = matrix_CN_2(lmbda,mx+2)
    D2[0,1] = lmbda ; D2[-1,-2] = lmbda
    
    
    #Initial set of points u at time t=0
    for i in range(0, mx+1):
        u_j[i] = init_cond(x[i])

    
    #Finding set of points u at u = u(x,T)
    for n in range(1, mt+1):
    
        b = matrix_RHS(mx,n-1,x,func_RHS)                             #Source term at each time increment

        u_jp1 = timestep_method(u_j,t,n,lmbda,deltax,deltat,b,p,q)    #Compute u at next time increment
          
        u_j = u_jp1

    return (x,u_j,deltat,deltax)

"""Prescribing inputs """

#Defining parameter values
kappa = 1.0 ; L = 1.0 ; T = 0.5 ; 
init_cond = u_I1
p = p1 ; q = q1

#Defining general timestep method and source term
timestep_method = BE_nonhomdir
matrix_RHS = matrix_RHS_dir
func_RHS = func_RHS_1

"""Finding errors """ 

#Setting up error and deltax vectors, and range of deltax
a = 2 ; b = 32
Es_x = np.zeros(b-a)
deltaxs = np.zeros(b-a)

#Finding how errors vary for change in deltax
for mx in range(a,b):
    mt=1000
    (x5,u_j5,deltat,deltax) = solve_parabolic_pde_general_error(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)
    uexact = u_exact(x5, T)
    deltaxs[mx-a] = deltax
    E = LA.norm(u_j5 - uexact)
    Es_x[mx-a] = E

#Setting up error and deltat vectors, and range of deltat
c = 1000 ; d=1051
Es_t = np.zeros(d-c)
deltats = np.zeros(d-c)

#Finding how errors vary for change in deltat
for mt in range(c,d):
    mx = 20
    (x6,u_j6,deltat_2,deltax_2) = solve_parabolic_pde_general_error(kappa,L,T,init_cond,mx,mt,timestep_method,matrix_RHS,p,q,func_RHS)
    uexact = u_exact(x6,T)
    deltats[mt-c] = deltat_2
    E = LA.norm(u_j6 - uexact)
    Es_t[mt-c] = E

"""Plotting errors"""

fig21,ax21 = pl.subplots()
ax21.plot(np.log(deltaxs),np.log(Es_x), np.log(deltaxs[8]),np.log(Es_x[8]), 'ro ', np.log(deltaxs[0]),np.log(Es_x[0]), 'ro')
ax21.set(xlabel = 'log(Deltax)', ylabel = 'log(error)', title = 'Log-log graph of truncation error against Deltax - Forward Euler')
gradient = (np.log(Es_x[8]) - np.log(Es_x[0]))/(np.log(deltaxs[8]) - np.log(deltaxs[0]))
#pl.savefig('error_FE.png')


fig22,ax22 = pl.subplots()
ax22.plot(np.log(deltats),np.log(Es_t))
ax22.set(xlabel = 'log(Deltat)', ylabel = 'log(error)', title = 'Log-log graph of truncation error against Deltat - Forward Euler')
gradient2 = (np.log(Es_t[-1]) - np.log(Es_t[0]))/(np.log(deltats[-1]) - np.log(deltats[0]))
#pl.savefig('error_BE_t.png')