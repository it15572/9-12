#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 12:09:02 2018

@author: imogentaylor
"""

"""Variable wave speed - modelling the movement of a tsunami using pdes"""
import numpy as np
from scipy.sparse import diags
import pylab as pl
from matplotlib import pyplot as plt
from matplotlib import animation

""" Sea bed and wave speed functions defined"""
def sea_bed_func(x,pars_B):
    return -a_B*np.exp(-((x-x_B)**2)/(omega_B**2))

def h_constant(x,pars_B):
    return h_0

def h_vary(x,pars_B):
    return h_0 + (sea_bed_func(x,pars_B))




"""Matrix used to calculate u_j+1 defined here """
def matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h):
    diag_0 = np.zeros(mx)
    diag_1 = np.zeros(mx+1)
    diag_2 = np.zeros(mx)
    for i in range(0,len(diag_0)):
        diag_0[i] = (Delta**2)*h(x[i] - deltax/2,pars_B)
        diag_2[i] = (Delta**2)*h(x[i] + deltax/2,pars_B)
    for i in range(0,len(diag_1)):
        diag_1[i] = 2 - (Delta**2)*(h(x[i] - deltax/2,pars_B) + h(x[i] + deltax/2,pars_B))
    diagonals = [diag_0,diag_1,diag_2]
    return diags(diagonals,[-1,0,1],format = 'csc')




"""Functions that use matrix above to calculate u_j+1 """
#open boundary conditions 
def cd_variable_open(u_j,u_jm1,e):
    u_jm1[0] = u_jm1[0]/e ; u_jm1[-1] = u_jm1[-1]/e
    return (A_v_gen).dot(u_j) - u_jm1

#periodic boundary conditions and constant wave speed
def cd_variable_per_constant(u_j,u_jm1,e):
    u_jm1[0] = u_jm1[0]/e ; u_jm1[-1] = u_jm1[-1]/e
    u_jp1 = (A_v_gen).dot(u_j) - u_jm1
    if u_jp1[-1] > h_0 + epsilon:
        u_jp1[0] = u_jp1[-1]
    else:
        u_jp1 = u_jp1
    return (u_jp1)

#periodic boundary conditions and variable wave speed
def cd_variable_per_vary(u_j,u_jm1,e):
    u_jm1[0] = u_jm1[0]/e ; u_jm1[-1] = u_jm1[-1]/e
    u_jp1 = (A_v_gen).dot(u_j) - u_jm1
    if u_jp1[-1] > epsilon:
        u_jp1[0] = u_jp1[-1]
    else:
        u_jp1 = u_jp1
    return (u_jp1)



""" Function used to calculate u_j+1"""
def timestep_1(u_jm1, deltat, V_I, deltax, Delta):
    c = np.zeros(len(u_jm1))
    c[0] = - V_I[0]*(Delta**2)*deltax*(h_0**0.5) ; c[-1] = - V_I[-1]*(Delta**2)*deltax*(h_0**0.5)
    return 0.5*(A_v).dot(u_jm1) + deltat*V_I + c




""" Main function producing u(x,T)"""
def solve_pde_hyperbolic_varyspeed(L,T,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h):
    x = np.linspace(0,L, mx+1)     # gridpoints in space
    t = np.linspace(0,T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = (h_0**(1/2))*deltat/deltax
    Delta = (deltat/deltax)
#    print("lambda=",lmbda)

    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    
    
    global A_v, A_v_gen, epsilon
    A_v = matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h)
    A_v[0,0] = A_v[-1,-1] = 2 - 2*(Delta**2)*h_0
    A_v[0,1] = A_v[-1,-2] = (Delta**2)*2*h_0
    A_v_gen = matrix_var_1(lmbda,mx,Delta,deltax,x,pars_B,h)
    e = 1 + 2*Delta*(h_0**0.5)
    A_v_gen[0,1] = (2*(Delta**2)*h_0)/e
    A_v_gen[-1,-2] = (2*(Delta**2)*h_0)/e
    A_v_gen[0,0] = (2*(1 + Delta*(h_0**0.5) - (Delta**2)*h_0))/e
    A_v_gen[-1,-1] = (2*(1 + Delta*(h_0**0.5) - (Delta**2)*h_0))/e
    epsilon = 0.5
    #Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = init_u(x[i],pars,pars_B,h)    
    #vector of initial velocities
    V_I = vector_v(x,init_v)
    #first time step:
    u_j = timestep_1(u_jm1, deltat, V_I, deltax, Delta)

    for n in range(2,mt+1):

        u_jp1 = timestep_method(u_j,u_jm1,e)
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]  
        
        
    return (x,u_j,deltax)




"""Set up initial conditions u_0 and initial velocity vector function """ 

def u_vary(x,pars,pars_B,h):
    return a_I*np.exp((-(x-x_I)**2)/(omega_I**2))

def u_constant(x,pars,pars_B,h):
    return h_0 + a_I*np.exp((-(x-x_I)**2)/(omega_I**2))

def u_t(x):
    return 0

def vector_v(x,init_v):
    b = np.zeros(len(x))
    for i in range(0,len(x)):
        a = init_v(x[i])
        b[i] = a
    return b

#Prescribe value of h_0, sea bed height
global h_0
h_0 = 10



"""Setting parameter values and plotting u(x,T) for two different systems """
"""TO TEST DIFFERENT SYSTEMS - MUST VARY 'init_u', 'timestep_method', and 'h' """

"""CONSTANT WAVESPEED, OPEN BC """
L = 20 ; T = 4
mx = 100 ; mt = 1000 ; 
a_I= 7 ; x_I = 6 ; omega_I = 1
a_B = 0.5 ; x_B = 17 ; omega_B = 0.5
pars = (a_I,x_I,omega_I)
pars_B = (a_B,x_B,omega_B)
init_v = u_t 


#Vary these
timestep_method = cd_variable_open ; init_u = u_constant ; h=h_constant
(x1,u_jp1,deltax) = solve_pde_hyperbolic_varyspeed(L,T,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h)

fig1,ax1 = pl.subplots()
ax1.plot(x1,u_jp1, 'r', label = 'num')
pl.legend(loc='upper right')
pl.axis([0, 20, -1, 20])
ax1.set(xlabel='x',ylabel='u(x,T)', title = 'Constant wave speed - open bcs')
pl.savefig('wave_open_constant.png')


"""VARIABLE WAVESPEED, PEIODIC BC """
L = 20 ; T = 4
mx = 100 ; mt = 1000 ;
a_I= 7 ; x_I = 6 ; omega_I = 1
a_B = 0.5 ; x_B = 15 ; omega_B = 0.5
pars = (a_I,x_I,omega_I)
pars_B = (a_B,x_B,omega_B)
init_v = u_t


#Vary these
timestep_method = cd_variable_per_vary ; init_u = u_vary  ; h=h_vary
(x2,u_jp2,deltax) = solve_pde_hyperbolic_varyspeed(L,T,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h)

fig2,ax2 = pl.subplots()
ax2.plot(x2,u_jp2 + h_0,'b',x2, sea_bed_func(x2,pars_B), 'orange')
pl.axis([0, 20, -1.5, 20])
ax2.set(xlabel='x',ylabel='u(x,T)', title = 'Variable wave speed - periodic bcs')
pl.savefig('wave_periodic_variable.png')




"""ANIMATIONS for two different systems above """
fig3 = plt.figure()
ax3 = plt.axes(xlim=(0, L), ylim=(-2, 20))
line, = ax3.plot([], [], lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    (x1,u_jp1,deltax) = solve_pde_hyperbolic_varyspeed(L,i,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h)
    line.set_data(x1, u_jp1)
    return line,

anim3 = animation.FuncAnimation(fig3, animate, init_func=init,
                               frames=200, interval=1, blit=True)

anim3.save('constantspeed_wave_opennew.htm', fps=200)

fig4,ax4 = pl.subplots()
pl.axis([0, 20, -1.5, 20])
line, = ax4.plot([], [], lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    (x2,u_jp2,deltax) = solve_pde_hyperbolic_varyspeed(L,i,mx,mt,timestep_method,init_u,init_v,pars,pars_B,h)
    line.set_data(x2, u_jp2+h_0)
    return line,

anim4 = animation.FuncAnimation(fig4, animate, init_func=init,
                               frames=200, interval=0.25, blit=True)

anim4.save('variablespeed_wave_periodiccorrect2.htm', fps=50)


