#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 11:17:10 2018

@author: imogentaylor
"""

# simple explicit finite difference solver for the 1D wave equation
#   u_tt = c^2 u_xx  0<x<L, 0<t<T
# with zero-displacement boundary conditions
#   u=0 at x=0,L, t>0
# and prescribed initial displacement and velocity
#   u=u_I(x), u_t=v_I(x)  0<=x<=L,t=0

import numpy as np
import pylab as plt
from math import pi
from scipy import sparse
from scipy.sparse import linalg



""" Matrices used in calculations """
def matrix_1(lmbda,mx):
    h = sparse.diags([lmbda**2,(2-2*(lmbda**2)),lmbda**2],[-1,0,1],shape = (mx-1,mx-1), format = 'csc')
    return h

def matrix_2(lmbda,mx):
    h = sparse.diags([-lmbda**2,2+2*lmbda**2,-lmbda**2],[-1,0,1],shape = (mx-1,mx-1), format = 'csc')
    return h

def matrix_3(lmbda,mx):
    h = sparse.diags([(-lmbda**2)/2,(1+(lmbda**2)),(-lmbda**2)/2],[-1,0,1],shape = (mx-1,mx-1), format = 'csc')
    return h
def matrix_4(lmbda,mx):
    h = sparse.diags([(lmbda**2)/2,(-1-(lmbda**2)),(lmbda**2)/2],[-1,0,1],shape = (mx-1,mx-1), format = 'csc')
    return h


"""Functions used to calculate u_jp1 in general timestep"""
def cd_conditional_dir(u_j,u_jm1,mx,t,n):
    a = np.zeros(mx+1)
    a[1:mx] = (A_h).dot(u_j[1:mx]) - u_jm1[1:mx]
    a[0] = p(t[n]) ; a[-1] = q(t[n])
    return a

def cd_unconditional_dir(u_j,u_jm1,mx,t,n):
    a = np.zeros(mx+1)
    a[1:mx] = linalg.spsolve(D_h,2*u_j[1:mx] + E_h.dot(u_jm1[1:mx]))
    a[0] = p(t[n]) ; a[-1] = q(t[n])
    return a

def cd_conditional_neum(u_j,u_jm1,mx,t,n):
    return (A_h_n).dot(u_j) - u_jm1

def cd_unconditional_neum(u_j,u_jm1,mx,t,n):
    return linalg.spsolve(D_h_n,2*u_j + E_h_n.dot(u_jm1))



"""Functions used to calculate u in first timestep"""
def time1_cond_dir(u_jm1,deltat,V_I,lmbda,mx,t):
    a = np.zeros(mx+1)
    a[1:mx] = 0.5*A_h.dot(u_jm1[1:mx]) + deltat*V_I[1:mx]
    a[0] = p(t[0]) ; a[-1] = q(t[0])
    return a 

def time1_uncond_dir(u_jm1,deltat,V_I,lmbda,mx,t):
    a = np.zeros(mx+1)
    a[1:mx] = linalg.spsolve(B_h, 2*u_jm1[1:mx] + deltat*B_h.dot(V_I[1:mx]))
    a[0] = p(t[0]) ; a[-1] = q(t[0])
    return a

#DIDN'T HAVE TIME TO FINISH IMPLEMENTING:
#def time1_uncond_nonhomdir(u_jm1,deltat,V_I,lmbda,mx,t):
#    c = np.zeros(mx)
#    c[0] = p(t[1]) ; c[-1] = q(t[1])
#    return linalg.spsolve(B_h, 2*u_jm1[1:mx] + deltat*B_h.dot(V_I[1:mx])) + (lmbda**2)*c

def time1_cond_neum(u_jm1,deltat,V_I,lmbda,mx,t):
    return (0.5)*(A_h_n.dot(u_jm1)) + deltat*V_I

def time1_uncond_neum(u_jm1,deltat,V_I,lmbda,mx,t):
    return linalg.spsolve(B_h_n,2*u_jm1 + deltat*B_h_n.dot(V_I))


""" Main function producing u(x,T)"""

def solve_hyperbolic_pde_general(c,L,T,mx,mt,timestep_method,time1_method,init_u,init_v):
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = c*deltat/deltax         # squared courant number
    print("lambda=",lmbda)

    # set up the solution variables
    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
  
    global A_h_n,B_h_n,D_h_n,E_h_n               #matrices used for neumann conditions
    A_h_n = matrix_1(lmbda,mx+2)
    A_h_n[0,1] = A_h_n[-1,-2] = 2*(lmbda**2)
    B_h_n = matrix_2(lmbda,mx+2)
    B_h_n[0,1] = B_h_n[-1,-2] = -2*(lmbda**2)
    D_h_n = matrix_3(lmbda,mx+2)
    D_h_n[0,1] = D_h_n[-1,-2] = -lmbda**2
    E_h_n = matrix_4(lmbda,mx+2)
    E_h_n[0,1] = E_h_n[-1,-2] = lmbda**2
    
    global A_h, B_h, C_h, D_h, E_h              #matrices used for dirichlet conditions
    A_h = matrix_1(lmbda,mx)
    B_h = matrix_2(lmbda,mx)
    D_h = matrix_3(lmbda,mx)
    E_h = matrix_4(lmbda,mx)
    
    #initial condition u_t(x,0) = v(x)
    V_I = vector_v(x,init_v)
    # Set initial condition - timestep zero - t=0
    for i in range(0, mx+1):
        u_jm1[i] = init_u(x[i])   

    #timestep one - t=1
    u_j = time1_method(u_jm1,deltat,V_I,lmbda,mx,t)
    
    
    #other timesteps
    for n in range(2, mt+1):
        
        
        #next timestep
        u_jp1 = timestep_method(u_j,u_jm1,mx,t,n)
        
        
        #update previous timestep values
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
    
    return (x,u_jp1)
    

"""Initial Displacements and Velocities"""
def u_I1(x):
    y = np.cos(pi*x/L)
    return y

def u_I2(x):
    return 0

def u_I3(x):
    y=A*np.sin(pi*x/L)
    return y

def v_I1(x):
    return 0 

def v_I2(x):
    y = np.cos(pi*x/L)
    return y 

def v_I3(x):
    y=A*np.sin(pi*x/L)
    return y

"""Dirichlet Conditions """
def p(t):
    return 0
def q(t):
    return 0 

""" Function that creates vector of initial velocities"""
def vector_v(x,init_v):
    b = np.zeros(len(x))
    for i in range(0,len(x)):
        a = init_v(x[i])
        b[i] = a
    return b
""" Functions producing exact solutions """
def u_exact(x,t):
    # the exact solution
    y = np.cos(pi*c*t/L)*np.cos(pi*x/L)
    return y

def u_exact2(x,t):
    y = A*L*(np.sin(pi*c*t/L)*np.sin(pi*x/L))/(pi*c)
    return y
    
"""Executing code for different systems """
"""TO TEST DIFFERENT SYSTEMS - MUST VARY 'timestep_method' and 'time1_method' in pairs """
#parameters
c = 1.0 ; L = 1.0 ; T = 1 ; mx = 500 ; mt = 400 ; A = 1
#CHANGE THESE FOR DIFFERENT METHODS
timestep_method = cd_unconditional_dir ; time1_method = time1_uncond_dir
init_u = u_I3 ; init_v = v_I1
(x1,ujp11) = solve_hyperbolic_pde_general(c,L,T,mx,mt,timestep_method,time1_method,init_u,init_v)
plt.plot(x1,ujp11, 'ro', label='numeric')
#xx = np.linspace(0,L,250)
#plt.plot(xx,u_exact2(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,T)')
plt.legend()
#plt.savefig('wave_uncond_dir_125')